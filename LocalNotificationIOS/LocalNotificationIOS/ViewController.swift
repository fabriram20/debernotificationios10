//
//  ViewController.swift
//  LocalNotificationIOS
//
//  Created by fabri on 3/7/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit
//Framework for notification
import UserNotifications

class ViewController: UIViewController {

    var isGrantedNotificationAccess:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Check for User Permissions
        UNUserNotificationCenter.current().requestAuthorization(
            options: [.alert,.sound,.badge],
            completionHandler: { (granted,error) in
                self.isGrantedNotificationAccess = granted
        }
        )
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func send10SecNotification(_ sender: UIButton) {
        
        if isGrantedNotificationAccess{
            //add notification code here
            let content = UNMutableNotificationContent()
            content.title = "10 Second Notification Demo"
            content.subtitle = "From MakeAppPie.com"
            content.body = "Notification after 10 seconds - Your pizza is Ready!!"
            content.categoryIdentifier = "message"
         
            //Adding trigger
            let trigger = UNTimeIntervalNotificationTrigger(
                timeInterval: 10.0,
                repeats: false)
            
            //Adding Notification Request
            let request = UNNotificationRequest(
                identifier: "10.second.message",
                content: content,
                trigger: trigger
            )
            
            //Adding the Notification
            UNUserNotificationCenter.current().add(
                request, withCompletionHandler: nil)
            
            
        }
        
        
    }
    


}

