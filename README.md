# TUTORIAL: LOCAL NOTIFICATION IOS 10 #

##Nombre: Fabrizio Ramirez

* Crear un botón, añadir constraints y vincularlo al View controller como una "Action" titulada "send10SecNotification"

* Añadir el framework "UserNotifications" para poder hacer uso de las notificaciones
 
* Solicitar al usuario que permita el uso de notificaciones y verificar que estas autorizaciones existan.

* Las notificaciones tiene tres opciones Alert, sound, Badge. Y se definen en forma de un array.

* Creamos una el contenido de la notificación (UNMutableNotificationContent) la cual debe tener un titulo, subtitulo, cuerpo y categoría.

* Configurar un Trigger que active la notificación luego de 10 segundos para ello definimos un objeto de tipo "UNTimeIntervalNotificationTrigger".

* Añadir un Notification Request que define el contenido y el trigger.

* añadir la notificación en el botón.

* Ejecutar la aplicación (luego de presionar el botón debemos salir de la app par ver la notificación).